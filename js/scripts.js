var modal = document.getElementById("myModal");
var modalImg = document.getElementById("img01");
var span = document.getElementsByClassName("closebotao")[0];
var video = document.querySelector("#video");
var imgv = document.querySelector(".mobile-img");



if (window.innerWidth == 849) {
    $(".navbar").addClass("navbar-expand-lg");
    $(".navbar").removeClass("navbar-expand-sm");
}



let get = document.querySelector.bind(document)

function busca(event) {
    event.preventDefault();
    let placa = get('#placa').value;
    let produto = get('#produto').value;

    validarPlaca(placa.toUpperCase(), produto.toUpperCase())
}


function formularioMobile(event) {
    event.preventDefault();
    let placa = get('#placaMobile').value;
    let produto = get("#produtoMobile").value;

    validarPlaca(placa.toUpperCase(), produto.toUpperCase())
}

function validarPlaca(placa, produto) {
    let regras = /[A-Z]{3}\d{4}$/.test(placa);
    if (!produto) {
        alert("produto invalido!");
        $(".bordainputproduto").addClass("cordaborda");
        $(".bordainputproduto").removeClass("form-control")
        $(".bordainputproduto").removeClass("cordaborda2");
    } else {
        $(".bordainputproduto").addClass("cordaborda2");
        $(".bordainputproduto").removeClass("form-control");
        $(".bordainputproduto").removeClass("cordaborda");

    }
    if (regras) {
        $(".bordainput").addClass("cordaborda2");
        $(".bordainput").removeClass("form-control")
        $(".bordainput").removeClass("cordaborda");
    } else {
        alert("placa invalida!");
        $(".bordainput").addClass("cordaborda");
        $(".bordainput").removeClass("form-control");
        $(".bordainput").removeClass("cordaborda2");

    }
    if (regras && produto != "") {
        $(".bordainput").addClass("cordaborda2");
        $(".bordainput").removeClass("form-control")
        $(".bordainputproduto").addClass("cordaborda2");
        $(".bordainputproduto").removeClass("form-control");
        localStorage.setItem('pesquisaapecon', 'true')
        return window.location.assign(`https://scario.bibipecas.com.br/search/${placa}/${produto}`)
    } else {
        $(".bordainput").addClass("cordaborda");
        $(".bordainput").removeClass("form-control");
        $(".bordainputproduto").addClass("cordaborda");
        $(".bordainputproduto").removeClass("form-control")

    }
}

$('.carousel').carousel({
    interval: 4000
  })
  