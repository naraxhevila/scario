class Controller {
    constructor() {
        let $ = document.querySelector.bind(document)
        this.navbar = new Navbar($("#form"));
        this.footer = new Footer($("#footer"));
     
        this.init();
      
        this.validacaoDesktop = new Bibi($("#placa"), $("#produto"));
       
    }
    
  
   envia(event) { return this.validacaoDesktop.init(event) }
   
    init() {
        this.navbar.update();
        this.footer.update();
      
        
      
    }
}