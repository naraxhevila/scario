class Slide extends View {
    constructor(elemento) {
        super(elemento)
        this.img1 = "img/carousel/slide1.jpg";
        this.img2 = "img/carousel/slide2.jpg";
        this.img3 = "img/carousel/slide3.jpg";
        this.img4 = "img/carousel/slide4.jpg";

    }

    template() {
        return `<div class="container-fluid" >
        <div class="row">
            <div id="slide" class="carousel slide" data-ride="carousel">
                <ol id="indicadores" class="carousel-indicators">
                    <li data-target="#slide" data-slide-to="0" class="active"></li>
                    <li data-target="#slide" data-slide-to="1"></li>
                    <li data-target="#slide" data-slide-to="2"></li>
                    <li data-target="#slide" data-slide-to="3"></li>
                </ol>

                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" src="${this.img1}" alt="Primeiro Slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="${this.img2}" alt="Segundo Slide">
                    </div>
                    <div class="carousel-item">
                        <a href="https://lista.mercadolivre.com.br/_CustId_40558673"><img class="d-block w-100" src="${this.img3}" alt="Terceiro Slide"></a>
                    </div>
                    <div class="carousel-item">
                        <a href="https://api.whatsapp.com/send?phone=5585998218229"><img class="d-block w-100" src="${this.img4}" alt="quarto Slide"></a>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#slide" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Anterior</span>
                </a>
                <a class="carousel-control-next" href="#slide" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Próximo</span>
                </a>
            </div>
        </div>
    </div>`
    }

}